# Decorrelated Sparse Survival Regression
# 
# Copyright (C) 2024
# Benjamin Paaßen
# AG Knowledge Representation and Machine Learning
# Bielefeld University

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2024, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'


import numpy as np
from scipy import sparse
import osqp
from survival_linprog import SurvivalRegressor

class QuadprogSurvivalRegressor(SurvivalRegressor):

  def __init__(self, regul_l1, regul_corr):
    super(QuadprogSurvivalRegressor, self).__init__(regul_l1, regul_corr)


  def fit_numpy(self, X, T, E = None, Z = None):
    """ Fits this model to the given survival data.

    Parameters
    ----------
    X: ndarray
      An N x n feature matrix.
    T: ndarray
      An N-element vector of survival times.
    E: ndarray (default = None)
      An N-element vector of status flags (0 for survival, 1 for dead).
    Z: ndarray (default = None)
      An N x m feature matrix that should be de-correlated from
      the scores.

    Returns
    -------
    self

    """
    N, n = X.shape
    if len(T) != N:
      raise ValueError('inconsistent shapes between X and T; expected the same number of samples')
    if E is not None:
      if len(E) != N:
        raise ValueError('inconsistent shapes between X and E; expected the same number of samples')

    # start setting up the sparse constraint matrix for the quadratic program
    data = []
    rows = []
    cols = []
    num_slack = 0
    for i in range(N):
      # ignore all samples on the left-hand side where we have a status
      # flag of 0 becaue in those cases we can not know whether T[i] < T[j]
      if E is not None and E[i] < 0.5:
        continue
      for j in range(N):
        # only take into account pairs (i, j) where i died before j,
        # i.e. pairs where T[i] < T[j]
        if T[i] >= T[j]:
          continue
        # start with the difference of features between i and j
        for k in range(n):
          data.append(X[i, k] - X[j, k])
          rows.append(num_slack)
          cols.append(k)
        # add a one entry for the slack variable of the current pair
        data.append(1)
        rows.append(num_slack)
        cols.append(n+num_slack)
        # increment number of slack variables
        num_slack += 1

    # add identity matrix to side constraints to ensure that all
    # variables of the problem remain non-negative
    for k in range(n + num_slack):
      data.append(1)
      rows.append(num_slack + k)
      cols.append(k)

    # combine all sparse data to A matrix
    A = sparse.csc_array((data, (rows, cols)), shape = (2*num_slack + n, n + num_slack))

    # set up lower bound constraint vector
    l = np.concatenate((np.ones(num_slack), np.zeros(n + num_slack)), axis = 0)

    # set up upper bound constraint vector
    u = np.ones(2*num_slack + n) * np.inf

    # set up sparse coefficient matrix for the objective function
    if Z is not None:
      if Z.shape[0] != N:
        raise ValueError('inconsistent shapes between X and Z; expected the same number of samples')
      m = Z.shape[1]
      # if Z is given, add a small n x n dense block matrix on the top left
      w_block = np.zeros((n, n))
      for j in range(m):
        # consider jth column of Z, remove nan entries and normalize
        valid_j = np.isfinite(Z[:, j])
        if not np.any(valid_j):
          continue
        zj  = Z[valid_j, j]
        std_j = np.std(zj)
        if std_j < 1E-3:
          continue
        zj  = (zj - np.mean(zj)) / std_j
        # multiply with X
        zj  = zj @ X[valid_j, :]
        w_block += np.outer(zj, zj)
      w_block = self.regul_corr * w_block / N * num_slack
    else:
      m = 0
      w_block = None
    P = sparse.bmat([[w_block, None], [None, sparse.eye(num_slack)]], format='csc')

    # set up coefficient vector for objective function
    q = np.concatenate((self.regul_l1 * np.ones(n), np.zeros(num_slack)), axis = 0)

    # initialize quadratic program
    problem = osqp.OSQP()
    problem.setup(P, q, A, l, u)

    # Solve the problem
    sol = problem.solve()

    # retrieve solution
    if sol.info.status_val != 1:
      print('WARNING: quadratic programming was not successful with OSQP status flag %d' % sol.info.status_val)
    self.fun_ = sol.info.obj_val
    self.w_ = sol.x[:n]

