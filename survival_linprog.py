# Decorrelated Sparse Survival Regression
# 
# Copyright (C) 2024
# Benjamin Paaßen
# AG Knowledge Representation and Machine Learning
# Bielefeld University

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2024, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import numpy as np
from scipy.sparse import csr_array
from scipy.optimize import linprog
from lifelines.utils import concordance_index


class SurvivalRegressor:

  def __init__(self, regul_l1, regul_corr):
    self.regul_l1   = regul_l1
    self.regul_corr = regul_corr

  def fit(self, X, duration_col, event_col=None, Z = None):
    """ Fits the model to the given data.

    Parameters
    ----------
    X: pandas.DataFrame
      A pandas data frame with columns for survival time, event status
      (optional), and covariates of survival.
    duration_col: str
      The header of the column in X that indicates survival time.
    event_col: str (optional)
      The header of the column in X that indicates survival status, 0 for
      survival, 1 for dead.
    Z: pandas.DataFrame (optional)
      A second data frame containing columns that should _not_ correlate with
      the risk scores.

    """
    self.duration_col_ = duration_col
    self.event_col_ = event_col

    # extract times and events and convert everything to numpy
    T = X[duration_col].to_numpy()
    if event_col is not None:
      E = X[event_col].to_numpy()
      X = X.drop([duration_col, event_col], axis = 1)
    else:
      E = None
      X = X.drop([duration_col], axis = 1)
    X = X.to_numpy()
    if Z is not None:
      Z = Z.to_numpy()

    # call internal fit function
    self.fit_numpy(X, T, E, Z)

    # compute concordance index
    s = self.predict(X)
    self.concordance_index_ = concordance_index(T, -s, event_observed=E)
    # return
    return self


  def predict(self, X):
    if not isinstance(X, np.ndarray):
      if self.event_col_ is not None:
        X = X.drop([self.duration_col_, self.event_col_], axis = 1)
      else:
        X = X.drop([duration_col], axis = 1)
      X = X.to_numpy()
    return X @ self.w_


  def score(self, X, Z = None, scoring_method = 'concordance_index'):
    """ Scores the performance of this model on test data, using
    the concordance_index function of lifelines.

    If Z is given, this also computes the mean absolute spearman
    correlation between predicted risk scores and Z.

    Parameters
    ----------
    X: pandas.DataFrame
      A pandas data frame with the same column structure as the training data.
    Z: pandas.DataFrame (optional)
      A second data frame containing columns that should _not_ correlate with
      the risk scores.
    scoring_method: str (default = 'concordance_index')
      Is ignored. If this is anything but 'concordance_index', an error is
      thrown.

    Returns
    -------
    c: float
      The concordance index between predicted risk scores and actual survival
      data (as given in x).
    r: float (only if Z is given)
      The mean absolute spearman correlation between predicted risk scores and
      columns of Z.

    """
    if scoring_method != 'concordance_index':
      raise ValueError('Only concordance_index is supported as scoring method')

    s = self.predict(X)

    T = X[self.duration_col_]
    if self.event_col_ is not None:
      E = X[self.event_col_]
    else:
      E = None

    c = concordance_index(T, -s, event_observed=E)

    if Z is None:
      return c

    rs_spearman = []
    for feature in Z.columns.values.tolist():
        z = Z[feature].to_numpy()
        r, p = spearmanr(s[np.isfinite(z)], z[np.isfinite(z)])
        rs_spearman.append(np.abs(r))

    return c, np.mean(rs_spearman)



class LinprogSurvivalRegressor(SurvivalRegressor):

  def __init__(self, regul_l1, regul_corr):
    super(LinprogSurvivalRegressor, self).__init__(regul_l1, regul_corr)


  def fit_numpy(self, X, T, E = None, Z = None):
    """ Fits this model to the given survival data.

    Parameters
    ----------
    X: ndarray
      An N x n feature matrix.
    T: ndarray
      An N-element vector of survival times.
    E: ndarray (default = None)
      An N-element vector of status flags (0 for survival, 1 for dead).
    Z: ndarray (default = None)
      An N x m feature matrix that should be de-correlated from
      the scores.

    Returns
    -------
    self

    """
    N, n = X.shape
    if len(T) != N:
      raise ValueError('inconsistent shapes between X and T; expected the same number of samples')
    if E is not None:
      if len(E) != N:
        raise ValueError('inconsistent shapes between X and E; expected the same number of samples')

    # start setting up the sparse constraint matrix for the linear program
    data = []
    rows = []
    cols = []
    num_slack = 0
    for i in range(N):
      # ignore all samples on the left-hand side where we have a status
      # flag of 0 becaue in those cases we can not know whether T[i] < T[j]
      if E is not None and E[i] < 0.5:
        continue
      for j in range(N):
        # only take into account pairs (i, j) where i died before j,
        # i.e. pairs where T[i] < T[j]
        if T[i] >= T[j]:
          continue
        # start with the difference of features between i and j
        for k in range(n):
          data.append(X[j, k] - X[i, k])
          rows.append(num_slack)
          cols.append(k)
        # add a negative one entry for the slack variable of the current pair
        data.append(-1)
        rows.append(num_slack)
        cols.append(n+num_slack)
        # increment number of slack variables
        num_slack += 1

    # set up side constraint vector
    b = -np.ones(num_slack)

    # set up objective function
    c = np.concatenate((self.regul_l1 * np.ones(n), np.ones(num_slack) / num_slack), axis = 0)

    # if given, add side constraints for decorrelation. In particular,
    # we have a single slack variable that expresses the maximum over all
    # correlations and which we wish to minimize
    if Z is not None:
      if Z.shape[0] != N:
        raise ValueError('inconsistent shapes between X and Z; expected the same number of samples')
      m = Z.shape[1]
      for j in range(m):
        # consider jth column of Z, remove nan entries and normalize
        valid_j = np.isfinite(Z[:, j])
        if not np.any(valid_j):
          continue
        zj  = Z[valid_j, j]
        std_j = np.std(zj)
        if std_j < 1E-3:
          continue
        zj  = (zj - np.mean(zj)) / std_j
        # multiply with X
        zj  = zj @ X[valid_j, :] / N
        # for positive correlation
        for k in range(n):
          data.append(zj[k])
          rows.append(num_slack + 2*j)
          cols.append(k)
        data.append(-1)
        rows.append(num_slack + 2*j)
        cols.append(n+num_slack)
        # for negative correlation
        for k in range(n):
          data.append(-zj[k])
          rows.append(num_slack + 2*j+1)
          cols.append(k)
        data.append(-1)
        rows.append(num_slack + 2*j+1)
        cols.append(n+num_slack)

      # update the bounds vector
      b = np.concatenate((b, np.zeros(2*m)), axis = 0)

      # update objective function coefficients
      c = np.append(c, self.regul_corr)
    else:
      m = 0

    # combine all sparse data to A matrix
    A = csr_array((data, (rows, cols)), shape = (num_slack + 2*m, n + num_slack + np.sign(m)))

    # start linear programming
    sol = linprog(c, A, b, bounds = (0., None))

    # retrieve solution
    if not sol.success:
      print('WARNING: linear programming was not successful with message %s' % sol.message)
    self.fun_ = sol.fun
    self.w_ = sol.x[:n]

