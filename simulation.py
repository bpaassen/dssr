# Decorrelated Sparse Survival Regression
# 
# Copyright (C) 2024
# Benjamin Paaßen
# AG Knowledge Representation and Machine Learning
# Bielefeld University

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2024, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import numpy as np
import pandas as pd

def generate_dataset(N, n, m):
    # sample ground-truth risk scores
    scores  = (0.1 + 0.9 * np.random.rand(N))
    # sample ground truth times until death from day of examination  
    times   = np.random.exponential(120 / scores)
    # compute 'progression times' by subtracting expected value
    prog    = 120 / scores - times

    # sample observable features. First those that scale with the actual risk
    cols_grading = ['grading%d' % j for j in range(n)]
    X        = np.zeros((N, n+m))
    X[:, :n] = np.expand_dims(scores, 1) + np.random.randn(N,n) * 0.1
    # then those that scale with the progression, instead
    cols_staging = ['staging%d' % j for j in range(m)]
    X[:, n:] = np.expand_dims(prog / 365, 1) + np.random.randn(N,m) * 0.1

    Z       = np.copy(X[:, n:])
    Z       = pd.DataFrame(Z, columns = cols_staging)

    data    = np.concatenate((X, np.expand_dims(times, 1), np.ones((N, 1))), axis = 1)
    data    = pd.DataFrame(data, columns = cols_grading + cols_staging + ['times', 'event'])

    return data, scores, Z
