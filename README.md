# Decorrelated Sparse Survival Regression

Copyright (C) 2024 - Benjamin Paassen
AG Knowledge Representation and Machine Learning
Bielefeld University

This is an implementation of decorrelated sparse survival regression as described in the ESANN2024 submission "Tumor Grading via Decorrelated Sparse Survival Regression".

## Getting Started

Please install the requirements as outlined in the `requirements.txt`. Then, you should be able to execute the `simulation.ipynb` to reproduce the simulation experiments described in the paper.

The real-world data can, unfortunately, not be included in this distribution due to privacy regulations.
