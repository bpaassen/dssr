# Decorrelated Sparse Survival Regression
# 
# Copyright (C) 2024
# Benjamin Paaßen
# AG Knowledge Representation and Machine Learning
# Bielefeld University

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright (C) 2024, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'bpaassen@techfak.uni-bielefeld.de'

import cma
import numpy as np
from lifelines.utils import concordance_index
from scipy.stats import spearmanr

class CMAESSurvivalFitter:
  """ A CMAES optimizer to find weights that, when multiplied with input
  features, yield risk scores that
  1. maximize the concordance index with given survival data.
  2. minimize correlation with given separate features.

  Attributes
  ----------
  regul_l1: float
    The L1 regularization strength to be employed during optimization.
  regul_corr: float (default = 1.)
    The regularization strength for the (squared) Spearman's correlation
    between predicted risk scores and separate features.
  init_w: float (default = 1.)
    The initial value for all weights.
  init_sigma: float (default = 0.2)
    The initial standard deviation for all parameters
  maxfevals: int (default = 50000)
    The maximum number of function evaluations during training.

  """

  def __init__(self, regul_l1, regul_corr = 1., init_w = 1., init_sigma = 0.2, maxfevals = 5E4):
    self.regul_l1 = regul_l1
    self.regul_corr = regul_corr
    self.init_w = init_w
    self.init_sigma = init_sigma
    self.maxfevals = maxfevals

  def fit(self, X, duration_col, event_col=None, Z = None):
    """ Fits the model to the given data.

    Parameters
    ----------
    X: pandas.DataFrame
      A pandas data frame with columns for survival time, event status
      (optional), and covariates of survival.
    duration_col: str
      The header of the column in X that indicates survival time.
    event_col: str (optional)
      The header of the column in X that indicates survival status, 0 for
      survival, 1 for dead.
    Z: pandas.DataFrame (optional)
      A second data frame containing columns that should _not_ correlate with
      the risk scores.

    """
    self.duration_col_ = duration_col
    self.event_col_ = event_col
    # extract times and events and convert everything to numpy
    T = X[duration_col].to_numpy()
    if event_col is not None:
      E = X[event_col].to_numpy()
      X = X.drop([duration_col, event_col], axis = 1)
    else:
      E = None
      X = X.drop([duration_col], axis = 1)
    X = X.to_numpy()
    if Z is not None:
      Z = Z.to_numpy()

    # set up objective function
    def objective_func(w_raw):
        # transform w to be positive via a square transformation
        w = w_raw ** 2
        # multiply with X to obtain the socres for every patient
        s = np.dot(X, w)
        # compute (negative) concordance index as first part of
        # the objective function
        loss  = -concordance_index(T, -s, event_observed=E)
        # L1 regularization (but on the raw params)
        # to promote sparser solutions
        loss += self.regul_l1 * np.sum(np.abs(w_raw)) / np.sqrt(np.sum(w))
        # punish undesired correlations of scores
        corrs = np.zeros(Z.shape[1])
        for j in range(Z.shape[1]):
            z = Z[:, j]
            finj = np.isfinite(z)
            if np.any(finj):
              r, p = spearmanr(s[np.isfinite(z)], z[np.isfinite(z)])
              corrs[j] = r
        # add the maximum absolute spearman correlation to the loss
        loss += self.regul_corr * np.max(np.abs(corrs))
        return loss

    # set the initial weights
    if isinstance(self.init_w, np.ndarray):
      init_w = self.init_w
    else:
      init_w = X.shape[1] * [self.init_w]

    # initialize the evolutionary optimizer
    es = cma.CMAEvolutionStrategy(init_w, self.init_sigma,
      {'maxfevals': self.maxfevals})
    # start the optimization process
    es.optimize(objective_func)

    # retrieve found weights
    w = es.result.xbest ** 2

    # normalize scaling
    self.w_ = w / np.sum(w)

    s     = np.dot(X, self.w_)
    self.concordance_index_ = concordance_index(T, -s, event_observed=E)

  def predict(self, X):
    """ Predicts risk scores on the given data.

    Parameters
    ----------
    X: pandas.DataFrame
      A pandas data frame with the same column structure as the training data.

    Returns
    -------
    s: numpy.ndarray
      The vector of risk scores for all input data.

    """

    if self.event_col_ is not None:
      X = X.drop([self.duration_col_, self.event_col_], axis = 1)
    else:
      X = X.drop([duration_col], axis = 1)
    X = X.to_numpy()
    return np.dot(X, self.w_)


  def score(self, X, Z = None, scoring_method = 'concordance_index'):
    """ Scores the performance of this model on test data, using
    the concordance_index function of lifelines.

    If Z is given, this also computes the mean absolute spearman
    correlation between predicted risk scores and Z.

    Parameters
    ----------
    X: pandas.DataFrame
      A pandas data frame with the same column structure as the training data.
    Z: pandas.DataFrame (optional)
      A second data frame containing columns that should _not_ correlate with
      the risk scores.
    scoring_method: str (default = 'concordance_index')
      Is ignored. If this is anything but 'concordance_index', an error is
      thrown.

    Returns
    -------
    c: float
      The concordance index between predicted risk scores and actual survival
      data (as given in x).
    r: float (only if Z is given)
      The mean absolute spearman correlation between predicted risk scores and
      columns of Z.

    """
    if scoring_method != 'concordance_index':
      raise ValueError('Only concordance_index is supported as scoring method')

    s = self.predict(X)

    T = X[self.duration_col_]
    if self.event_col_ is not None:
      E = X[self.event_col_]
    else:
      E = None

    c = concordance_index(T, -s, event_observed=E)

    if Z is None:
      return c

    rs_spearman = []
    for feature in Z.columns.values.tolist():
        z = Z[feature].to_numpy()
        r, p = spearmanr(s[np.isfinite(z)], z[np.isfinite(z)])
        rs_spearman.append(np.abs(r))

    return c, np.mean(rs_spearman)
